#!/usr/bin/env python3
import sys
import slixmpp

opts = {
  "to":       "resource",
  "jid":      "botname@example.com",
  "resource": "gsend",
  "password": "password",
  "connect":  "xmpp.example.org:5222",
}


def on_failed_all_auth(event):
    print("Auth: Could not connect to the server, or password mismatch!")
    sys.exit(1)


def on_session_start(event):
    # client.send_presence()
    client.get_roster()

    body = "hptoad_self_message: %s" % "\n".join(sys.argv[1:]).strip()
    try:
        if body:
            recipient = "%s/%s" % (opts["jid"], opts["to"])
            client.send_message(mto=recipient, mbody=body, mtype="chat")
    except Exception as e:
        print("%s: %s" % (type(e).__name__, str(e)))
    finally:
        client.disconnect()


def on_session_end(event):
    sys.exit(0)


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("At least one argument is required.")
        sys.exit(1)

    client = slixmpp.ClientXMPP("%s/%s" % (opts["jid"], opts["resource"]),
                                opts["password"])

    if opts["connect"]:
         connect = opts["connect"].split(":", 1)
         if len(connect) != 2 or not connect[1].isdigit():
             print("Connection server format is invalid, should be " +
                   "example.org:5222")
             sys.exit(1)
    else:
        connect = ()

    client.connect(connect)
    client.add_event_handler("failed_all_auth", on_failed_all_auth)
    client.add_event_handler("session_start", on_session_start)
    client.add_event_handler("session_end", on_session_end)
    client.process(forever=True)
