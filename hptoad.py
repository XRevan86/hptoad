#!/usr/bin/env python3
import asyncio
import functools
import importlib.util
import logging
import os
import re
import signal
import sys
import types
import slixmpp

opts = {
  "muc":      "room@conference.example.com",
  "status":   "is there some food in this world?",
  "nick":     "botname",
  "jid":      "botname@example.com",
  "resource": "resource",
  "password": "password",
  "connect":  "xmpp.example.org:5222",
}


class HptoadPlugin:
    name = None

    def __init__(self, name):
        spec = importlib.util.find_spec("plugins.%s" % name)
        module = types.ModuleType(spec.name)
        spec.loader.exec_module(module)

        self.name = "plugins.%s" % name
        self._obj = module.Plugin()

    async def _call(self, cb_name, *args, **kwargs):
        ret = {}
        try:
            if hasattr(self._obj, cb_name):
                func = getattr(self._obj, cb_name)
                if asyncio.iscoroutinefunction(func):
                    ret = await func(*args, **kwargs)
                else:
                    ret = func(*args, **kwargs)
                if not ret or type(ret) != dict:
                    ret = {}
        except Exception as e:
            ret = {"error": "%s: %s" % (type(e).__name__, str(e))}
        return {"handled": bool(ret.get("handled", False)),
                "reply": str(ret.get("reply", "")),
                "error": str(ret.get("error", ""))}

    async def call_initiate(self):
        result = await self._call("initiate")
        return result

    async def call_question(self, body, nick, from_id, is_admin):
        result = await self._call("question", body, nick, from_id, is_admin)
        return result

    async def call_command(self, command, body, nick, from_id, is_admin):
        result = await self._call("command", command, body, nick,
                                  from_id, is_admin)
        return result

    async def call_chat_message(self, body, nick, from_id, is_admin):
        result = await self._call("chat_message", body, nick,
                                  from_id, is_admin)
        return result


class HptoadXMPP:
    def __init__(self, hptoad, timeout, jid, resource, password,
                 connect, muc, status, bot_nick):
        self.hptoad = hptoad
        self.client = slixmpp.ClientXMPP("%s/%s" % (jid, resource), password)

        self.client.register_plugin("xep_0199")  # XMPP Ping.
        self.client_ping = self.client.plugin["xep_0199"]
        self.client.register_plugin("xep_0045")  # XMPP MUC.
        self.client_muc = self.client.plugin["xep_0045"]

        self.client_ping.timeout = self.timeout = timeout
        self.client_ping.keepalive = True
        self.muc_is_joined = False

        self.conf_jid = jid
        self.conf_resource = resource
        self.conf_password = password
        self.conf_muc = muc
        self.conf_status = status
        self.conf_bot_nick = bot_nick
        self.conf_connect = connect
        self.bot_nick = self.conf_bot_nick

        self._register_handlers()

    def _register_handlers(self):
        self.client.add_event_handler("failed_all_auth",
                                      self.on_failed_all_auth)
        self.client.add_event_handler("session_start", self.on_session_start)
        self.client.add_event_handler("got_online", self.on_got_online)
        self.client.add_event_handler("disconnected", self.on_disconnected)
        self.client.add_event_handler("message", self.on_message)
        self.client.add_event_handler("muc::%s::presence" % self.conf_muc,
                                      self.on_muc_presence)
        self.client.add_event_handler("muc::%s::message_error" % self.conf_muc,
                                      self.on_muc_message_error)

    def _join_muc(self):
        async def loop_cycle():
            if self._join_muc_lock.locked():
                return

            await self._join_muc_lock
            try:
                while not self.muc_is_joined:
                    self.client_muc.join_muc(self.conf_muc, self.bot_nick)
                    await asyncio.sleep(self.timeout)
            except Exception as e:
                self.hptoad.log_exception(e)
            finally:
                self._join_muc_lock.release()

        asyncio.ensure_future(loop_cycle())
    _join_muc_lock = asyncio.Lock()

    def on_failed_all_auth(self, event):
        asyncio.ensure_future(self.hptoad.handle_xmpp_failed_auth())

    def on_session_start(self, event):
        self.client.get_roster()
        self.client.send_presence(pshow="", pstatus=self.conf_status,
                                  ppriority=12)

    def on_got_online(self, event):
        self._join_muc()

    async def on_disconnected(self, event):
        self.muc_is_joined = False
        await self.hptoad.handle_disconnected()

    async def on_message(self, event):
        try:
            if not event["type"] in ("chat", "normal", "groupchat"):
                return
            body = event["body"]
            from_id = event["from"]

            self.log_message_event(event["from"], event["id"], event["to"],
                                   event["type"], body)

            if event["type"] == "groupchat":
                nick = event["mucnick"]
                if nick != self.bot_nick:
                    await self.hptoad.handle_muc_message(body, nick, from_id)

            elif event["from"].bare == self.conf_jid:
                # Use resource as a nickname with self messages.
                nick = event["from"].resource
                await self.hptoad.handle_self_message(body, nick, from_id)
        except Exception as e:
            self.hptoad.log_exception(e)

    async def on_muc_presence(self, event):
        try:
            typ = event["muc"]["type"]
            nick = event["muc"]["nick"]

            if not typ:
                typ = event["type"]
            if not nick:
                nick = self.client_muc.get_nick(self.conf_muc, event["from"])

            if typ == "available":
                if nick == self.bot_nick:
                    self.muc_is_joined = True

            elif typ in ("unavailable", "error"):
                if nick == self.bot_nick:
                    self.muc_is_joined = False

                    if typ == "unavailable":
                        self.bot_nick = self.conf_bot_nick
                    elif typ == "error":
                        if event["error"]["code"] == "409":
                            self.bot_nick += "_"

                    await asyncio.sleep(0.5)
                    self._join_muc()
        except Exception as e:
            self.hptoad.log_exception(e)

    async def on_muc_message_error(self, event):
        try:
            to_nick = event["from"].resource
            to_id = self.client_muc.get_jid_property(self.conf_muc, to_nick, "jid")
            body = event["body"]
            if to_id:
                self.client.send_message(mto=to_id, mbody=body, mtype="chat")
        except Exception as e:
            self.hptoad.log_exception(e)

    def get_bot_nick(self):
        return self.bot_nick

    def list_users(self):
        return tuple(self.client_muc.rooms[self.conf_muc])

    def kick_user(self, nick, reason=""):
        self.client_muc.set_role(self.conf_muc, nick, "none")

    def is_admin(self, nick):
        if nick not in self.list_users():
            return False

        affiliation = self.client_muc.get_jid_property(self.conf_muc, nick,
                                                       "affiliation")
        return True if affiliation in ("admin", "owner") else False

    def send_private_message(self, body, to_nick):
        to_id = "%s/%s" % (self.conf_muc, to_nick)
        self.client.send_message(mto=to_id, mbody=body, mtype="chat")

    def send_message(self, body):
        self.client.send_message(mto=self.conf_muc, mbody=body,
                                 mtype="groupchat")

    def log_message_event(self, from_id, msg_id, to_id, msg_type, body):
        self.hptoad.log_event("&{{jabber:client message} " \
                              "%s %s %s %s  %s  {  }}" %
                              (from_id, msg_id, to_id, msg_type, body))

    async def run(self):
        # Reset the nick.
        self.bot_nick = self.conf_bot_nick

        if self.conf_connect:
            connect = self.conf_connect.split(":", 1)
            if len(connect) != 2 or not connect[1].isdigit():
                return False
        else:
            connect = ()

        self.client.connect(connect)
        return True


class Hptoad:
    def __init__(self, opts, timeout=5.0):
        self.plugins = {}
        self.timeout = timeout

        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.addHandler(logging.NullHandler())
        self.logger.setLevel(logging.DEBUG)

        self.xmpp = HptoadXMPP(self, timeout, opts["jid"], opts["resource"],
                                opts["password"], opts["connect"],
                                opts["muc"], opts["status"], opts["nick"])

    def on_plugin_result(self, future, nick="", from_id="", is_admin=False):
        result = future.result()
        if not result:
            return

        if result["handled"] and result["reply"]:
            self.xmpp.send_message(result["reply"])
        elif result["error"] and nick:
            self.xmpp.send_message("%s: FAIL" % nick)
        elif result["error"]:
            self.xmpp.send_message("FAIL")

        if result["error"]:
            self.logger.error(result["error"])
            if is_admin and nick:
                self.xmpp.send_private_message(result["error"], nick)

    async def handle_command(self, command, body, nick, from_id, is_admin):
        futures = []

        if command == "megakick":  # Megakick.
            reply = None
            victim = body
            if victim:
                is_bot_admin = self.xmpp.is_admin(self.xmpp.get_bot_nick())
                is_victim_admin = self.xmpp.is_admin(victim)

                if is_admin and victim != self.xmpp.get_bot_nick():
                    if is_bot_admin and not is_victim_admin and \
                       victim in self.xmpp.list_users():
                        self.xmpp.kick_user(victim, "You got megakicked!")
                    else:
                        reply = "%s: Can't megakick %s." % (nick, victim)
                else:
                    reply = "%s: GTFO" % nick
            else:
                reply = "%s: WAT" % nick

            if reply:
                self.xmpp.send_message(reply)

        else:  # Any plugin command.
            for plugin in self.plugins.values():
                generator = plugin.call_command(command, body, nick,
                                                from_id, is_admin)
                futures.append(asyncio.ensure_future(generator))

        return futures

    async def handle_self_message(self, body, nick, from_id):
        if body.startswith("hptoad_self_message: "):
            body = body[len("hptoad_self_message: "):]
        else:
            return

        if body.startswith("!"):
            split = body.split(" ", 1)
            command = split[0].strip()[1:]
            message = split[1] if len(split) > 1 else ""

            futures = await self.handle_command(command, message, nick, True)
            if futures:
                for future in futures:
                    callback = functools.partial(self.on_plugin_result,
                                                 nick=nick, from_id=from_id,
                                                 is_admin=True)
                    future.add_done_callback(callback)

                results = await asyncio.gather(*futures)
                if not [i["handled"] for i in results if i["handled"]]:
                    self.xmpp.send_message("%s: WAT" % nick)

        elif body and len(body) > 0:
            self.xmpp.send_message(body.strip())

    async def handle_muc_message(self, body, nick, from_id):
        is_admin = self.xmpp.is_admin(nick)
        futures = []
        plugin_futures = []

        # Has to be redone with the current bot nick.
        call_regexp = re.compile("^%s[:,]" %
                                 re.escape(self.xmpp.get_bot_nick()))

        for plugin in self.plugins.values():
            generator = plugin.call_chat_message(body, nick, from_id, is_admin)
            futures.append(asyncio.ensure_future(generator))

        if body.startswith("!"):  # Any plugin command.
            split = body.split(" ", 1)
            command = split[0].strip()[1:]
            message = split[1] if len(split) > 1 else ""

            plugin_futures = await self.handle_command(command, message, nick,
                                                       from_id, is_admin)
            futures.extend(plugin_futures)

        elif call_regexp.match(body):  # Chat.
            message = call_regexp.sub("", body).lstrip()
            for plugin in self.plugins.values():
                generator = plugin.call_question(message, nick,
                                                 from_id, is_admin)
                futures.append(asyncio.ensure_future(generator))

        if futures:
            for future in futures:
                callback = functools.partial(self.on_plugin_result,
                                             nick=nick, from_id=from_id,
                                             is_admin=is_admin)
                future.add_done_callback(callback)

            if plugin_futures:
                results = await asyncio.gather(*plugin_futures)
                if not [i["handled"] for i in results if i["handled"]]:
                    self.xmpp.send_message("%s: WAT" % nick)

            await asyncio.gather(*futures)

    async def handle_xmpp_failed_auth(self):
        self.logger.critical("Auth: Could not connect to the server, or " +
                             "password mismatch!")
        sys.exit(1)

    async def handle_disconnected(self):
        self.logger.error("Conn: Connection lost, reattempting in %d seconds" %
                          self.timeout)
        await asyncio.sleep(self.timeout)
        await self.xmpp.run()

    def import_plugins(self):
        plugins = {}
        _, _, filenames = next(os.walk("./plugins"), (None, None, []))
        for plugin_name in (i[:-3] for i in filenames if i.endswith(".py")):
            try:
                plugin = HptoadPlugin(plugin_name)
                plugins[plugin_name] = plugin
                future = asyncio.ensure_future(plugin.call_initiate())
                future.add_done_callback(self.on_plugin_result)
            except Exception as e:
                self.log_exception(e)
        self.plugins = plugins

    def log_event(self, text):
        self.logger.debug(text)

    def log_exception(self, ex):
        self.logger.error("%s: %s" % (type(ex).__name__, str(ex)))

    async def run(self):
        self.import_plugins()
        if not (await self.xmpp.run()):
            self.logger.critical("Conn: Connection server format is " +
                                 "invalid, should be example.org:5222")
            sys.exit(1)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    logging.basicConfig(format="%(asctime)s %(message)s",
                        datefmt="%Y/%m/%d %H:%M:%S")

    if os.path.isfile(sys.argv[0]) and os.path.dirname(sys.argv[0]):
        os.chdir(os.path.dirname(sys.argv[0]))

    hptoad = Hptoad(opts)
    asyncio.ensure_future(hptoad.run())
    asyncio.get_event_loop().run_forever()
