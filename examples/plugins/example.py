# -*- python -*-

class Plugin:
    def command(self, command, body, nick, from_id, is_admin):
        if command != "example":
            return {"handled": False}

        if is_admin:
            reply = "%s: you are an admin" % nick
        else:
            reply = "%s: you are not an admin" % nick
        return {"handled": True, "reply": reply}
